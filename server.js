require('dotenv').config()
const stripe = require('stripe')(process.env.STRIPE_SK);

const express = require("express");
const app = express();

app.use(express.static("public"));
app.use(express.json());

const calculateOrderAmount = (items) => {
    return 1400;
};

let allTransac = [];

app.post("/create-payment-intent", async (req, res) => {
    const { items } = req.body;

    // Create a PaymentIntent with the order amount and currency
    const paymentIntent = await stripe.paymentIntents.create({
        amount: calculateOrderAmount(items),
        currency: "eur",
        payment_method_types: [
            "giropay",
            "eps",
            "p24",
            "sofort",
            "sepa_debit",
            "card",
            "bancontact",
            "ideal",
        ],
    });

    let temp = {};
    temp.transacId = paymentIntent.id;
    temp.amount = paymentIntent.amount;
    allTransac.push(temp);
    console.log(allTransac);

    res.send({
        clientSecret: paymentIntent.client_secret,
    });
});
app.listen(3000, () => console.log("Node server listening on port 3000!"));